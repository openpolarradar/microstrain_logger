// g++ -I/usr/share/c++-mscl/source -I/usr/share/c++-mscl/Boost/include test.cpp -o test -L/usr/share/c++-mscl -lmscl -lstdc++ -std=c++11 -lpthread

#include <stdio.h>

#include <iostream>
using namespace std;

#include "mscl/mscl.h"

int main(int argc, char *argv[])
{
	printf("Test\n");

	const string COM_PORT = "/dev/ttyACM0";
	mscl::Connection connection = mscl::Connection::Serial(COM_PORT);

	try
	{
		mscl::InertialNode node(connection);

		cout << "Node Information: " << endl;
		cout << "Model Name: " << node.modelName() << endl;
		cout << "Model Number: " << node.modelNumber() << endl;
		cout << "Serial: " << node.serialNumber() << endl;
		cout << "Firmware: " << node.firmwareVersion().str() << endl << endl;

		printf("%s:%d: Test\n", __FILE__, __LINE__);
		mscl::GpioConfiguration cc;
		printf("%s:%d: Test\n", __FILE__, __LINE__);
		cc.pin = 3;
		printf("%s:%d: Test\n", __FILE__, __LINE__);
		cc.feature = mscl::GpioConfiguration::PPS_FEATURE;
		printf("%s:%d: Test\n", __FILE__, __LINE__);
		//node.setGpioConfig(cc);
		printf("%s:%d: Test\n", __FILE__, __LINE__);


		mscl::GpioConfiguration cc2;
		cc2 = node.getGpioConfig(1);
		printf("%s:%d: pin %d\n", __FILE__, __LINE__, cc2.pin);
		printf("%s:%d: feature %d\n", __FILE__, __LINE__, cc2.feature);
		printf("%s:%d: pinMode %ld\n", __FILE__, __LINE__, cc2.pinMode.value());
		printf("%s:%d: behavior %d\n", __FILE__, __LINE__, cc2.behavior);


		cc2 = node.getGpioConfig(3);
		printf("%s:%d: pin %d\n", __FILE__, __LINE__, cc2.pin);
		printf("%s:%d: feature %d\n", __FILE__, __LINE__, cc2.feature);
		printf("%s:%d: pinMode %ld\n", __FILE__, __LINE__, cc2.pinMode.value());
		printf("%s:%d: behavior %d\n", __FILE__, __LINE__, cc2.behavior);

		cc2.feature = mscl::GpioConfiguration::PPS_FEATURE;
		cc2.behavior = mscl::GpioConfiguration::PPS_OUTPUT;
		node.setGpioConfig(cc2);
		cc2 = node.getGpioConfig(1);
		printf("%s:%d: pin %d\n", __FILE__, __LINE__, cc2.pin);
		printf("%s:%d: feature %d\n", __FILE__, __LINE__, cc2.feature);
		printf("%s:%d: pinMode %ld\n", __FILE__, __LINE__, cc2.pinMode.value());
		printf("%s:%d: behavior %d\n", __FILE__, __LINE__, cc2.behavior);

		bool success = node.ping();
		printf("%s:%d: success %d\n", __FILE__, __LINE__, success);
		node.setToIdle();
		printf("%s:%d: success %d\n", __FILE__, __LINE__, success);
		mscl::MipChannels activeChs = node.getActiveChannelFields(mscl::MipTypes::CLASS_GNSS1);

mscl::MipChannels ahrsImuChs;
		printf("%s:%d: Test\n", __FILE__, __LINE__);
ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_RAW_ACCEL_VEC, mscl::SampleRate::Hertz(500)));
		printf("%s:%d: Test\n", __FILE__, __LINE__);
ahrsImuChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_SENSOR_RAW_GYRO_VEC, mscl::SampleRate::Hertz(100)));
		printf("%s:%d: Test\n", __FILE__, __LINE__);

		printf("%s:%d: Test\n", __FILE__, __LINE__);
mscl::MipChannels gnssChs;
		printf("%s:%d: Test\n", __FILE__, __LINE__);
gnssChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_GNSS_1_LLH_POSITION, mscl::SampleRate::Hertz(1)));
gnssChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_GNSS_1_UTC_TIME, mscl::SampleRate::Hertz(1)));
		printf("%s:%d: Test\n", __FILE__, __LINE__);

		printf("%s:%d: Test\n", __FILE__, __LINE__);
mscl::MipChannels estFilterChs;
		printf("%s:%d: Test\n", __FILE__, __LINE__);
estFilterChs.push_back(mscl::MipChannel(mscl::MipTypes::CH_FIELD_ESTFILTER_ESTIMATED_GYRO_BIAS, mscl::SampleRate::Hertz(100)));
		printf("%s:%d: Test\n", __FILE__, __LINE__);

		printf("%s:%d: Test\n", __FILE__, __LINE__);
//set the active channels for the different data classes on the Node
		printf("%s:%d: Test\n", __FILE__, __LINE__);
//node.setActiveChannelFields(mscl::MipTypes::CLASS_AHRS_IMU, ahrsImuChs);
		printf("%s:%d: Test\n", __FILE__, __LINE__);
node.setActiveChannelFields(mscl::MipTypes::CLASS_GNSS1, gnssChs);
		printf("%s:%d: Test\n", __FILE__, __LINE__);
//node.setActiveChannelFields(mscl::MipTypes::CLASS_ESTFILTER, estFilterChs);
		printf("%s:%d: Test\n", __FILE__, __LINE__);

		//node.setActiveChannelFields(mscl::MipTypes::CLASS_GNSS, gnssChs);
		printf("%s:%d: Test\n", __FILE__, __LINE__);
		node.enableDataStream(mscl::MipTypes::CLASS_GNSS1);
		printf("%s:%d: Test\n", __FILE__, __LINE__);

		for (int i=0; i < 10; i++)
		{
		printf("%s:%d: 111Test\n", __FILE__, __LINE__);
			//get all the packets that have been collected, with a timeout of 500 milliseconds
			mscl::MipDataPackets packets = node.getDataPackets(500);

		printf("%s:%d: Test\n", __FILE__, __LINE__);
			for(mscl::MipDataPacket packet : packets)
			{
				packet.descriptorSet(); //the descriptor set of the packet
				packet.collectedTimestamp();     //the PC time when this packet was received

		printf("%s:%d: inside\n", __FILE__, __LINE__);
				mscl::Timestamp tt;
				tt.setTimeNow();
				std::cout << tt.str();

		printf("%s:%d: inside\n", __FILE__, __LINE__);
		std::cout << packet.deviceTimestamp().str();

		printf("%s:%d: inside\n", __FILE__, __LINE__);
		std::cout << packet.shared().gpsTimestamp().str();

				//get all of the points in the packet
				mscl::MipDataPoints points = packet.data();

				for(mscl::MipDataPoint dataPoint : points)
				{
		printf("%s:%d: inside\n", __FILE__, __LINE__);
		std::cout << dataPoint.channelName();  //the name of the channel for this point
		std::cout << dataPoint.storedAs();     //the ValueType that the data is stored as
//std::cout << dataPoint.as_float();     //get the value as a float
		std::cout << dataPoint.as_string();     //get the value as a float
				}
			}
		}
	}
	catch(mscl::Error& e)
	{
		cout << "Error: " << e.what() << endl;
	}

	return(0);
}
